import java.awt.Container;
import java.awt.GridLayout;
import java.util.Hashtable;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class App extends JFrame{
    private static final long serialVersionUID = 1L;
    Container container;
    GUI gui = new GUI();  //отделяю графику от логики
    SendWindow sendWindow; //отдельное модульное окно для отправки сообщения на почту
    Hashtable<String, String> hashtableYesterday;
    Hashtable<String, String> hashtableToday;

    //2 таблицы имён, чтобы сделать небольшой выпадающий список
    Hashtable<String, String> yesterdayName = new Hashtable<>();
    Hashtable<String, String> todayName= new Hashtable<>();
    boolean day = true;  //true = today; false = yesterday

    App(Hashtable<String, String> hashtableYesterday, Hashtable<String, String> hashtableToday){
        super("HashTable Comparator");

        this.hashtableYesterday = hashtableYesterday;
        this.hashtableToday = hashtableToday;
        sendWindow = new SendWindow(this);
        sendWindow.setVisible(false);

        container = getContentPane();
        container.add(gui);


        //начальные значения
        setBounds(100, 50, 1280, 815);
        setLayout(new GridLayout(1, 1));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        gui.todayButton.setEnabled(false);



        //Делегирование кнопок
        gui.todayButton.addActionListener(new dayButtonsList());
        gui.yesterdayButton.addActionListener(new dayButtonsList());
        gui.compareButton.addActionListener(new compareButtonList());
        gui.URLPicker.addActionListener(new URLPickList());
        sendWindow.sendButton.addActionListener(new sendButtonList());
        



        //заполнение таблицы имён
        for ( String key : hashtableToday.keySet()){
            String name = key.substring(key.lastIndexOf("/")+1);
            todayName.put(name, key);
        }
        for ( String key : hashtableYesterday.keySet()){
            String name = key.substring(key.lastIndexOf("/")+1);
            yesterdayName.put(name, key);
        }
        //добавление имён в выпадающий список
        for ( String key : todayName.keySet()){
            gui.URLPicker.addItem(key);
        }

    }

    //Ниже классы Listener'ов
    // при переключении дня
    private class dayButtonsList implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton) e.getSource();
            gui.URLPicker.removeAllItems();
            button.setEnabled(false);
            day = !day;
                
            if(button == gui.todayButton){
                gui.yesterdayButton.setEnabled(true);
                for ( String key : todayName.keySet()){
                    gui.URLPicker.addItem(key);
                }
            }
            else if(button == gui.yesterdayButton){
                gui.todayButton.setEnabled(true);
                for ( String key : yesterdayName.keySet()){
                    gui.URLPicker.addItem(key);
                }
            }
            
        }
   
    }

    // при выборе какой-то страницы
    private class URLPickList implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JComboBox<String> jcb = (JComboBox<String>)e.getSource();
            String name = (String) jcb.getSelectedItem();
            if(name != null){
                if(day) gui.browser.setURL(todayName.get(name));
                else    gui.browser.setURL(yesterdayName.get(name));
            }
        }
   
    }

    // при нажатии на кнопку сравнения
    private class compareButtonList implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            sendWindow.restart();
            sendWindow.setVisible(true);
        }
    }

    // при нажатии кнопки отправить
    private class sendButtonList implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            HashTableComparator.compare(hashtableYesterday, hashtableToday);

            boolean flag = HashTableComparator.sendMSG(sendWindow.getMail());
            if(flag){
                sendWindow.Error();
            }
            else {
                sendWindow.Sucsess();
            }


        }
    }
}