import java.io.File;
import java.io.IOException;

import javax.swing.JEditorPane;

public class Browser extends JEditorPane{
    
    private static final long serialVersionUID = 1L;
    File file;

    Browser(){
        setEditable(false);
    }

    // Думал передавать путь, но так как у нас дан URL, сразу передаётся URL
    public void setURL(String URL){
            try {
                setPage(URL);
              }catch (IOException e2) {
                setContentType("text/html");
                setText("<html>Could not load</html>");
            }
    }
    
}