/*Hashtable comparator by Ilya Obydennye
* В коде используется библиотека javax.mail и javax.activation
* .jar файлы приложены
*
* для GUI используется библиотека Swing*/
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

public class Main {
    private static Hashtable<String, String> hashtableYesterday = new Hashtable<String, String>();
    private static Hashtable<String, String> hashtableToday = new Hashtable<String, String>();
    public static void main(String[] args){
        //заполняем таблицы
        File folder = new File("site/today");
        listFilesForFolder(folder, hashtableToday);
        folder = new File("site/yesterday");
        listFilesForFolder(folder, hashtableYesterday);

        //запускаем основную программу
        App app = new App(hashtableYesterday, hashtableToday);
    }

    
    // Считать все html файлы из папок и их пути в hashtable. Так как по заданию hashtable уже есть,
    // я заполняю их из произвольных html файлов и передаю в класс с основной программой
    private static void listFilesForFolder(final File folder, Hashtable<String, String> hashtable) {  
        for (final File fileEntry : folder.listFiles()) {  
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, hashtable);
            } else {
                try {
                    FileReader fr = new FileReader(fileEntry);
                    String text = new String();
                    int a;
                    a = fr.read();
                    while(a != -1){
                        text += (char)a;
                        a = fr.read();
                    }
                    hashtable.put(fileEntry.toURI().toURL().toString(), text);
                    fr.close();
                } catch (IOException e) {
                    System.out.println("can't read file: " + fileEntry.getName());
                }

                    
            }
        }
    }
}
