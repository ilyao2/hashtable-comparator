import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class GUI extends JPanel{
    private static final long serialVersionUID = 1L;
    private JPanel chooserPanel = new JPanel(); // панель для управления
    private JPanel HTMLWrapper = new JPanel(); // Подложка для браузера
    public JButton compareButton = new JButton("Compare");
    private JPanel dayButtonPanel = new JPanel();
    public JButton todayButton = new JButton("Today");
    public JButton yesterdayButton = new JButton("Yesterday");
    private JPanel pickURLPanel = new JPanel(); //панель для выпадающего списка
    public JComboBox<String> URLPicker = new JComboBox<String>();
    public Browser browser = new Browser(); // мини браузер для отображение html кода
    GUI(){
        setBackground(Color.DARK_GRAY);
        setLayout(new BorderLayout());
        add(chooserPanel, BorderLayout.WEST);
        add(HTMLWrapper, BorderLayout.CENTER);
        add(compareButton, BorderLayout.SOUTH);
        
        chooserPanel.setBackground(Color.LIGHT_GRAY);
        chooserPanel.setLayout(new BorderLayout());
        chooserPanel.add(dayButtonPanel, BorderLayout.NORTH);
        chooserPanel.add(pickURLPanel, BorderLayout.CENTER);

        dayButtonPanel.add(yesterdayButton);
        dayButtonPanel.add(todayButton);

        pickURLPanel.add(URLPicker, BorderLayout.CENTER);
        pickURLPanel.setPreferredSize(new Dimension(150, 50));

        HTMLWrapper.setBackground(Color.WHITE);
        HTMLWrapper.setLayout(new BorderLayout());
        HTMLWrapper.add( new JScrollPane( browser ), BorderLayout.CENTER );

        compareButton.setBackground(Color.BLUE);
    }
}