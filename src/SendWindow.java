import javax.swing.*;
import java.awt.*;

public class SendWindow extends JDialog {
    private static final long serialVersionUID = 1L;
    public JButton sendButton = new JButton("Отправить");
    private JTextField email = new JTextField();
    private JLabel error = new JLabel("Error!");
    private JLabel sucsess = new JLabel("Sucsess!");

    //никакой проверки на правильность введённых данных не делал
    //при неверно введённых данных сообщение не отправиться вернётся ошибка
    //об этом сообщит диалоговое окно

    //сделал окно модальным, потому что есть задержка перед отправкой
    //таким образом нельзя будет что-то натыкать пока сообщение отправляется
    //и будет оправданно небольшое провисание перед отправкой
    SendWindow(App owner){
        super(owner, "Подготовка к отправлению", true);


        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(250, 120);
        add(email);
        add(sendButton);
        setLayout(new GridLayout(2,1,2,2));
    }

    String getMail(){
        return email.getText();
    }

    void Error(){
        remove(email);
        remove(sendButton);
        email.setText("");
        add(error);
        repaint();
        revalidate();
    }

    void Sucsess(){
        remove(email);
        remove(sendButton);
        email.setText("");
        add(sucsess);
        repaint();
        revalidate();
    }

    public void restart(){
        remove(error);
        add(email);
        add(sendButton);
        repaint();
        revalidate();
    }
}
