import java.util.Hashtable;
import java.util.Vector;

// Решил построить класс на статических методах
// Потому что программа не предполагает сравнивать одновременно несколько разных таблиц
// Есть вчерашняя таблица и сегодняшняя и только их сравнивает метод класса и отправляет результат
public class HashTableComparator{
    static String result = new String();

    //Сравнение и заполнение итога
    //Не очень красивое решение, но опять же, потому что я работаю с локальными страницами из разных папок
    //Но с одинаковыми именами
    public static String compare(Hashtable<String, String> htb1, Hashtable<String, String> htb2){
        Vector<String> changed = new Vector<String>();
        Vector<String> lost = new Vector<String>();
        Vector<String> added = new Vector<String>();

        //TODOO: URL'ы должны быть одинаковы, но в данном случае я разложил в разные папки
        //Поэтому я достую именно имя файла
        for ( String key1 : htb1.keySet()){
            String name1 = key1.substring(key1.lastIndexOf("/")+1);
            boolean flag = false;
            //if(htb2.keySet().contains(key1)){   Так использовать с URL

            for ( String key2 : htb2.keySet()){
                String name2 = key2.substring(key2.lastIndexOf("/")+1);
                if(name1.equals(name2)){
                    boolean flag2 = htb1.get(key1).equals( htb2.get(key2) );
                    if(!flag2){
                        changed.add(key1);
                    }
                    flag = true;
                }
                /*else{
                    lost.add(key1);
                }*/
            }

            if(!flag){
                lost.add(key1);
            }
        }



        // Здесь так же, как и первый обход, но в другую сторону
        for ( String key2 : htb2.keySet()) {
            String name2 = key2.substring(key2.lastIndexOf("/") + 1);
            boolean flag = false;
            //if(htb2.keySet().contains(key1)){   Так использовать с URL

            for (String key1 : htb1.keySet()) {
                String name1 = key1.substring(key1.lastIndexOf("/") + 1);
                if (name1.equals(name2)) {
                    flag = true;
                }
            }

            if(!flag){
                added.add(key2);
            }
        }

        generateString(changed, lost, added);

        return result;
    }

    public static String getResult() {
        return result;
    }



    public static boolean sendMSG(String email){

        // создал случайное тестовое мыло на гугле, которое будет отправлять сообщение
        EmailSender es = new EmailSender("testForOleg1990@gmail.com", "q9wwwq8eee");
        try{
            es.send("Comparation", result, email);
        }
        catch (RuntimeException e){
            System.out.println("Сообщение не было отправлено");
            return true;
        }
        return false;
    }

    // вынес в отдельный метод, чтоб не засорять метод сравнения
    // формирование строки для отправления
    private static void generateString(Vector<String> changed, Vector<String> lost, Vector<String> added){
        result = "";
        int i = 1;
        result += "Здравствуйте, дорогая и.о. секретаря\n\n";
        result += "За последние сутки во вверенных Вам сайтах произошли следующие изменения:\n";
        if(!lost.isEmpty()){
            result += String.valueOf(i++) + ") Исчезли следующие страницы:\n";
            for (String url : lost){
                result += url;
                result += "\n";
            }
        }
        if(!added.isEmpty()){
            result += String.valueOf(i++) + ") Появились следующие страницы:\n";
            for (String url : added){
                result += url;
                result += "\n";
            }
        }
        if(!changed.isEmpty()){
            result += String.valueOf(i++) + ") Изменились следующие страницы:\n";
            for (String url : changed){
                result += url;
                result += "\n";
            }
        }

        if(i == 1){
            result += "Никаких изменений не произошло.\n";
        }
    }
}